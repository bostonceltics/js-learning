"use strict";

// const myPromise = new Promise((resolve, reject) => {
//   const error = true;
//   if (!error) {
//     resolve("resolve!");
//   } else {
//     reject("reject!");
//   }
// });

// console.log(myPromise);

// myPromise
//   .then((value) => {
//     console.log(value);
//   })
//   .then((newValue) => {
//     console.log(newValue);
//   })
//   .catch((err) => {
//     console.log(err);
//   });

const myNextPromise = new Promise((resolve, reject) => {
  setTimeout(function () {
    resolve("myNextPromise resolved!");
  }, 3000);
});

myNextPromise.then((value) => {
  console.log(value);
});
